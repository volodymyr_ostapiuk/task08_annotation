package com.ostapiuk;

import com.ostapiuk.annotation.AnnotatedClass;
import com.ostapiuk.reflection.InvokeMethod;

/**
 * <p>Class Main allows user to enter the program.</p>
 *
 * @author Volodymyr Ostapiuk ostap.volodya@gmail.com
 * @version 1.0
 * @since 2019-05-05
 */
public class Main {
    /**
     * Constructor allows create class.
     */
    private Main() {
    }

    /**
     * Method main allows to start the program.
     *
     * @param args null
     */
    public static void main(String[] args) throws Exception {
        AnnotatedClass annotatedClass = new AnnotatedClass();
        annotatedClass.getCharacteristics();
    }
}