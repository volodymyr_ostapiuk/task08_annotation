package com.ostapiuk.annotation;

import com.ostapiuk.reflection.ReflectionClass;

import java.lang.annotation.Annotation;
import java.util.Arrays;

@OwnAnnotation(subject = "OOP", result = 80)
public class AnnotatedClass {
    @OwnAnnotation(subject = "Math", result = 77)
    private String subject;

    @OwnAnnotation(result = 85)
    private int result;

    @OwnAnnotation
    private String[] info;

    public AnnotatedClass() {
    }

    public AnnotatedClass(String subject, int result, String[] info) {
        this.subject = subject;
        this.result = result;
        this.info = info;
    }

    public void getCharacteristics() throws Exception {
        printClassAnnotatedFields();
        System.out.println(toString());
        new ReflectionClass().getClassInfo(this);
    }

    private void printClassAnnotatedFields() {
        Class clazz = AnnotatedClass.class;
        Annotation[] annotations = clazz.getAnnotations();
        System.out.println("Annotation for this class: ");
        for (Annotation annotation : annotations) {
            OwnAnnotation ownAnnotation = (OwnAnnotation) annotation;
            System.out.println("Subject: " + ownAnnotation.subject());
            System.out.println("Result: " + ownAnnotation.result());
            System.out.println("Info: " + Arrays.toString(ownAnnotation.info()));
        }
    }

    @OwnAnnotation(result = 90)
    public void ownMethod1() {
        System.out.println("ownMethod1() is invoked.");
    }

    @OwnAnnotation(subject = "English")
    public void ownMethod2(String line, int... params) {
        System.out.println(line);
    }

    @OwnAnnotation
    public String ownMethod3(String... lines){
        System.out.println(Arrays.toString(lines));
        return toString();
    }

    @Override
    public String toString() {
        return "Your results: "
                + "subject = " + subject
                + ", points = " + result
                + ", info = " + Arrays.toString(info);
    }
}
