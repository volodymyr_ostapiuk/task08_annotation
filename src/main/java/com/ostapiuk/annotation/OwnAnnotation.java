package com.ostapiuk.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface OwnAnnotation {
    String subject() default "Subject name";

    int result() default 50;

    String[] info() default {"5 credits", "2 semester"};
}