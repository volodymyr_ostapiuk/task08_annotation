package com.ostapiuk.reflection;

import com.ostapiuk.annotation.OwnAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionClass {
    public void getClassInfo(Object object) throws Exception {
        System.out.println("\nInformation about received class:");
        Class<?> clazz = object.getClass();
        System.out.println("Class name: " + clazz.getSimpleName()
                + ", superclass name: " + clazz.getSuperclass());
        printClassConstructor(clazz);
        printClassFields(clazz);
        printClassMethods(clazz);
        getInvokedMethods(object);
    }

    private void printClassConstructor(Class<?> clazz) {
        System.out.println("\nClass constructors:");
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            System.out.println("Name: " + constructor.getName()
                    + ", parameter type: " + Arrays.toString(constructor.getParameterTypes()));
        }
    }

    private void printClassFields(Class<?> clazz) {
        System.out.println("\nClass fields: ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("Name: " + field.getName()
                    + ", type: " + field.getType());
            if (field.isAnnotationPresent(OwnAnnotation.class)) {
                System.out.println("Initialising from OwnAnnotation: "
                        + Arrays.toString(field.getDeclaredAnnotations()));
            }
        }
    }

    private void printClassMethods(Class<?> clazz) {
        System.out.println("\nClass methods:");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("Name: " + method.getName()
                    + ", parameter type: " + Arrays.toString(method.getParameterTypes())
                    + ", get return type: " + method.getReturnType());
        }
    }

    private void getInvokedMethods(Object object) throws Exception {
        InvokeMethod invokeMethod = new InvokeMethod();
        invokeMethod.printInvokeMethods(object);
    }
}
