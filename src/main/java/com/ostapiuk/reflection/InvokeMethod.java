package com.ostapiuk.reflection;

import com.ostapiuk.annotation.AnnotatedClass;
import com.ostapiuk.annotation.OwnAnnotation;

import java.lang.reflect.Method;

public class InvokeMethod {
    private Class<AnnotatedClass> clazz = AnnotatedClass.class;
    private Method myMethod1 = clazz.getMethod("ownMethod1");
    private Method myMethod2 = clazz.getMethod("ownMethod2", String.class, int[].class);
    private Method myMethod3 = clazz.getMethod("ownMethod3", String[].class);

    public InvokeMethod() throws NoSuchMethodException {
    }

    public void printInvokeMethods(Object object) throws Exception {
        System.out.println("\nInvoked methods: ");
        if(myMethod1.isAnnotationPresent(OwnAnnotation.class)){
            myMethod1.invoke(object);
            myMethod2.invoke(object, "ownMethod2() is invoked.", new int[]{});
            String result = (String) myMethod3.invoke(object,
                    (Object) new String[]{"ownMethod3() is invoked."});
        }
    }
}
